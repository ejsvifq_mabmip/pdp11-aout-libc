#pragma once

#define PDP11_AOUT_LIBC_VERSION 20211222L

#if defined(__cplusplus)
#if __cplusplus >= 201103L
#define PDP11_AOUT_LIBC_CXX_NOEXCEPT noexcept
#else
#define PDP11_AOUT_LIBC_CXX_NOEXCEPT throw()
#endif
#else
#define PDP11_AOUT_LIBC_CXX_NOEXCEPT
#endif

#if defined(__cplusplus)
	#if __cplusplus >=201103L 
		#ifndef	PDP11_AOUT_LIBC_NULL
		#define	PDP11_AOUT_LIBC_NULL	nullptr
		#endif
	#else
		#ifndef	PDP11_AOUT_LIBC_NULL
			#define	PDP11_AOUT_LIBC_NULL	0
		#endif
	#endif
#else
	#ifndef	PDP11_AOUT_LIBC_NULL
		#define	PDP11_AOUT_LIBC_NULL	0
	#endif
#endif

#ifndef PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE
#if defined(__cplusplus)
#define PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE inline
#else
#define PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE __attribute__((gnu_inline))
#endif
#endif

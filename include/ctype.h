/*	ctype.h	4.2	85/09/04	*/
#pragma once

#if defined(__cplusplus)
extern "C"
{
#endif
#include <features.h>
#define	_U	01
#define	_L	02
#define	_N	04
#define	_S	010
#define _P	020
#define _C	040
#define _X	0100
#define	_B	0200

extern	char	_ctype_[];

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int isalpha(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return (_ctype_+1)[c]&(_U|_L);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int isupper(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&_U);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int islower(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&_L);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int isdigit(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&_N);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int isxdigit(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&(_N|_X));
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int isspace(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&_S);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int ispunct(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&_P);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int isalnum(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&(_U|_L|_N));
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int isprint(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&(_P|_U|_L|_N|_B));
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int isgraph(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&(_P|_U|_L|_N));
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int iscntrl(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((_ctype_+1)[c]&_C);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int isascii(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((unsigned)(c)<=0177);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int toupper(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((c)-'a'+'A');
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int tolower(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((c)-'A'+'a');
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int toascii(int c) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((c)&0177);
}

#if defined(__cplusplus)
}
#endif

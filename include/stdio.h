/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)stdio.h	5.3.2 (2.11BSD) 1997/7/29
 */
#ifndef	_STDIO_H_
#define	_STDIO_H_

#if defined(__cplusplus)
extern "C"
{
#endif

#include<features.h>
#include<stddef.h>
#include<stdint.h>
#include<stdarg.h>

#define	BUFSIZ	1024
typedef struct _iobuf{
	int	_cnt;
	char	*_ptr;		/* should be unsigned char */
	char	*_base;		/* ditto */
	int	_bufsiz;
	short	_flag;
	char	_file;		/* should be short */
} FILE;

extern _iobuf _iob[];

#define	_IOREAD	01
#define	_IOWRT	02
#define	_IONBF	04
#define	_IOMYBUF	010
#define	_IOEOF	020
#define	_IOERR	040
#define	_IOSTRG	0100
#define	_IOLBF	0200
#define	_IORW	0400

/*
 * The following definition is for ANSI C, which took them
 * from System V, which brilliantly took internal interface macros and
 * made them official arguments to setvbuf(), without renaming them.
 * Hence, these ugly _IOxxx names are *supposed* to appear in user code.
*/
#define	_IOFBF	0	/* setvbuf should set fully buffered */
			/* _IONBF and _IOLBF are used from the flags above */

typedef int_least32_t fpos_t;

#ifndef NULL
#define NULL PDP11_AOUT_LIBC_NULL
#endif

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#define	EOF	(-1)
#define	stdin	(&_iob[0])
#define	stdout	(&_iob[1])
#define	stderr	(&_iob[2])

extern int _filbuf(FILE*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

extern int _flsbuf(int,FILE* __restrict) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int fgetc(FILE* __restrict p) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return --(p)->_cnt>=0? (int)(*(unsigned char *)(p)->_ptr++):_filbuf(p);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int getc(FILE* __restrict p) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return fgetc(p);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int getchar() PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return getc(stdin);
}

#define PDP11_AOUT_LIBC_PUTC(x, p)	(--(p)->_cnt >= 0 ?\
	(int)(*(unsigned char *)(p)->_ptr++ = (x)) :\
	(((p)->_flag & _IOLBF) && -(p)->_cnt < (p)->_bufsiz ?\
		((*(p)->_ptr = (x)) != '\n' ?\
			(int)(*(unsigned char *)(p)->_ptr++) :\
			_flsbuf(*(unsigned char *)(p)->_ptr, p)) :\
		_flsbuf((unsigned char)(x), p)))

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int putc(int x,FILE* __restrict p) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return PDP11_AOUT_LIBC_PUTC(x,p);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int putchar(int x) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return putc(x,stdout);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int feof(FILE* p) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return (((p)->_flag&_IOEOF)!=0);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int ferror(FILE* __restrict p) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return (((p)->_flag&_IOERR)!=0);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE int fileno(FILE* __restrict p) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return ((p)->_file);
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE void clearerr(FILE* __restrict p) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	((p)->_flag &= ~(_IOERR|_IOEOF));
}

FILE	*fdopen(int,char const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
FILE	*popen(char const*,char const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

int remove(const char* filename) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int rename(const char* old_p, const char* new_p) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
FILE* tmpfile() PDP11_AOUT_LIBC_CXX_NOEXCEPT;
char* tmpnam(char* s) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int fclose(FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int fflush(FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
FILE* fopen(const char* filename, const char* mode) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
FILE* freopen(const char* filename, const char* mode, FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void setbuf(FILE* stream, char* buf) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int setvbuf(FILE* stream, char* buf, int mode, size_t size) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int fprintf(FILE* stream, const char* format, ...) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int fscanf(FILE* stream, const char* format, ...) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int printf(const char* format, ...) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int scanf(const char* format, ...) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int snprintf(char* s, size_t n, const char* format, ...) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int sprintf(char* s, const char* format, ...) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int sscanf(const char* s, const char* format, ...) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int vfprintf(FILE* stream, const char* format, va_list arg) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int vfscanf(FILE* stream, const char* format, va_list arg) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int vprintf(const char* format, va_list arg) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int vscanf(const char* format, va_list arg) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int vsnprintf(char* s, size_t n, const char* format, va_list arg) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int vsprintf(char* s, const char* format, va_list arg) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int vsscanf(const char* s, const char* format, va_list arg) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
char* fgets(char* s, int n, FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int fputc(int c, FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int fputs(const char* s, FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int puts(const char* s) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int ungetc(int c, FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
size_t fread(void* ptr, size_t size, size_t nmemb, FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
size_t fwrite(const void* ptr, size_t size, size_t nmemb, FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int fgetpos(FILE* stream, fpos_t* pos) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int fseek(FILE* stream, long int offset, int whence) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int fsetpos(FILE* stream, const fpos_t* pos) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long int ftell(FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void rewind(FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int feof(FILE* stream) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void perror(const char* s) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
char *gets( char *str ) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

#if defined(__cplusplus)
}
#endif
#endif

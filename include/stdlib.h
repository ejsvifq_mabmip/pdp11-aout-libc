/*-
 * Copyright (c) 1990, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)stdlib.h	8.3.2 (2.11BSD) 1996/1/12
 *
 * Adapted from the 4.4-Lite CD.  The odds of a ANSI C compiler for 2.11BSD
 * being slipped under the door are not distinguishable from 0 - so the 
 * prototypes and ANSI ifdefs have been removed from this file. 
 *
 * Some functions (strtoul for example) do not exist yet but are retained in
 * this file because additions to libc.a are anticipated shortly.
 */

#ifndef _STDLIB_H_
#define _STDLIB_H_
#if defined(__cplusplus)
extern "C"
{
#endif
#include<features.h>
#include<stddef.h>
#include<stdint.h>

/* #include <machine/ansi.h> */

typedef struct {
	int quot;		/* quotient */
	int rem;		/* remainder */
} div_t;

typedef struct {
	long quot;		/* quotient */
	long rem;		/* remainder */
} ldiv_t;

typedef struct {
	long long quot;		/* quotient */
	long long rem;		/* remainder */
} lldiv_t;

#define	EXIT_FAILURE	1
#define	EXIT_SUCCESS	0

#define	RAND_MAX	0x7fff

void	abort() PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int	abs(int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int	atexit(void (*)()) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
double	atof(char const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int	atoi(char const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int	atol(char const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void	*calloc(size_t,size_t) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void	exit(int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void	free(void*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
char	*getenv(char const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void	*malloc(size_t) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void	qsort(void*, size_t, size_t,int (*)(const void *, const void *)) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int	rand() PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void	*realloc(void*, size_t) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
void	srand(int unsigned) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
double	strtod(char const*,char**) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long	strtol(char const*,char**,int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
unsigned long strtoul(char const*,char**,int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int	system(char const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
char* getenv(char const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

void* bsearch( const void *key, const void *ptr, size_t count, size_t size,
               int (*comp)(const void*, const void*) ) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE div_t div( int x, int y ) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return {x/y,x%y};
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE ldiv_t ldiv( long x, long y ) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return {x/y,x%y};
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE lldiv_t lldiv( long long x, long long y ) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	return {x/y,x%y};
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE long labs( long x ) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	if(x<0L)
		return (long)(0UL- (unsigned long)x);
	return x;
}

PDP11_AOUT_LIBC_GNU_INLINE_LINKAGE long long llabs( long long x ) PDP11_AOUT_LIBC_CXX_NOEXCEPT
{
	if(x<0LL)
		return (long long)(0ULL- (unsigned long long)x);
	return x;
}

#if defined(__cplusplus)
}
#endif
#endif /* _STDLIB_H_ */

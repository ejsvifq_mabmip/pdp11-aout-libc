/*
 * Copyright (c) 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)math.h	1.1 (2.10BSD Berkeley) 12/1/86
 */
#pragma once
#if defined(__cplusplus)
extern "C"
{
#endif
#include<features.h>

double      acos(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       acosf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double acosl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      acosh(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       acoshf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double acoshl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      asin(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       asinf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double asinl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      asinh(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       asinhf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double asinhl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      atan(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       atanf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double atanl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      atan2(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       atan2f(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double atan2l(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      atanh(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       atanhf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double atanhl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      cbrt(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       cbrtf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double cbrtl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      ceil(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       ceilf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double ceill(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      copysign(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       copysignf(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double copysignl(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      cos(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       cosf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double cosl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      cosh(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       coshf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double coshl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      erf(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       erff(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double erfl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      erfc(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       erfcf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double erfcl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      exp(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       expf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double expl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      exp2(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       exp2f(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double exp2l(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      expm1(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       expm1f(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double expm1l(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      fabs(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       fabsf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double fabsl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      fdim(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       fdimf(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double fdiml(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      floor(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       floorf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double floorl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      fma(double, double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       fmaf(float, float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double fmal(long double, long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      fmax(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       fmaxf(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double fmaxl(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      fmin(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       fminf(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double fminl(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      fmod(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       fmodf(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double fmodl(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      frexp(double, int *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       frexpf(float, int *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double frexpl(long double, int *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      hypot(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       hypotf(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double hypotl(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

int         ilogb(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int         ilogbf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
int         ilogbl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      ldexp(double, int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       ldexpf(float, int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double ldexpl(long double, int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      lgamma(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       lgammaf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double lgammal(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

long long   llrint(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long long   llrintf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long long   llrintl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

long long   llround(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long long   llroundf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long long   llroundl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      log(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       logf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double logl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      log10(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       log10f(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double log10l(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      log1p(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       log1pf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double log1pl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      log2(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       log2f(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double log2l(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      logb(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       logbf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double logbl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

long        lrint(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long        lrintf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long        lrintl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

long        lround(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long        lroundf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long        lroundl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      modf(double, double *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       modff(float, float *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double modfl(long double, long double *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      nan(const char *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       nanf(const char *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double nanl(const char *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      nearbyint(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       nearbyintf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double nearbyintl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      nextafter(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       nextafterf(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double nextafterl(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      nexttoward(double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       nexttowardf(float, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double nexttowardl(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      pow(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       powf(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double powl(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      remainder(double, double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       remainderf(float, float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double remainderl(long double, long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      remquo(double, double, int *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       remquof(float, float, int *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double remquol(long double, long double, int *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      rint(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       rintf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double rintl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      round(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       roundf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double roundl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      scalbln(double, long) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       scalblnf(float, long) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double scalblnl(long double, long) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      scalbn(double, int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       scalbnf(float, int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double scalbnl(long double, int) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      sin(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       sinf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double sinl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      sinh(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       sinhf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double sinhl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      sqrt(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       sqrtf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double sqrtl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      tan(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       tanf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double tanl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      tanh(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       tanhf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double tanhl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      tgamma(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       tgammaf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double tgammal(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

double      trunc(double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
float       truncf(float) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
long double truncl(long double) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

#define	HUGE	1.701411733192644270e38
#define	LOGHUGE	39

#define MATH_ERRNO  1
#define MATH_ERREXCEPT 2
#define math_errhandling 2

#define FP_ILOGBNAN (-1-0x7fffffff)
#define FP_ILOGB0 FP_ILOGBNAN

#define FP_NAN       0
#define FP_INFINITE  1
#define FP_ZERO      2
#define FP_SUBNORMAL 3
#define FP_NORMAL    4

#ifdef __FP_FAST_FMA
#define FP_FAST_FMA 1
#endif

#ifdef __FP_FAST_FMAF
#define FP_FAST_FMAF 1
#endif

#ifdef __FP_FAST_FMAL
#define FP_FAST_FMAL 1
#endif


#if defined(__cplusplus)
}
#endif

/*
 * Copyright (c) 1983, 1987 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)time.h	1.3 (2.11BSD) 96/7/10
 */
/*
 * Structure returned by gmtime and localtime calls (see ctime(3)).
 */
#if defined(__cplusplus)
extern "C"
{
#endif
#include<features.h>
#include<stdint.h>
#include<stddef.h>

typedef uint_least32_t time_t;
typedef uint_least32_t clock_t;

struct tm {
	int	tm_sec;
	int	tm_min;
	int	tm_hour;
	int	tm_mday;
	int	tm_mon;
	int	tm_year;
	int	tm_wday;
	int	tm_yday;
	int	tm_isdst;
	long	tm_gmtoff;
	char	*tm_zone;
};
struct tm *gmtime (time_t const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
struct tm *localtime (time_t const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

char *asctime(struct tm const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
char *ctime(time_t const*) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
time_t time(time_t *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
clock_t clock (void) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
double difftime (time_t, time_t) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
time_t mktime (struct tm *) PDP11_AOUT_LIBC_CXX_NOEXCEPT;
size_t strftime (char *__restrict, size_t, char const*__restrict, struct tm const*__restrict) PDP11_AOUT_LIBC_CXX_NOEXCEPT;

#if defined(__cplusplus)
}
#endif

/*	varargs.h	4.1	83/05/03	*/
#pragma once
#if defined(__cplusplus)
extern "C"
{
#endif
typedef char *va_list;
# define va_dcl int va_alist;
# define va_start(list) list = (char *) &va_alist
# define va_end(list)
# define va_arg(list,mode) ((mode *)(list += sizeof(mode)))[-1]
#if defined(__cplusplus)
}
#endif
